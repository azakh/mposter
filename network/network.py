"""
    Работа с url, доменами и ip адресами
"""
import ipaddress
import socket
from ipaddress import ip_address


def get_ip4s(addr: str, port=993) -> str:
    """
    Загружает ip адрес по домену
    """
    for (*_, (ip4, *_)) in socket.getaddrinfo(addr, port):
        if is_ip4(ip4):
            return ip4

    raise Exception(f"Not found if for host {addr}")


def is_ip4(ip4: bytes | str) -> bool:
    """Проверка на соответствие формату ip"""
    try:
        return isinstance(ip_address(ip4), ipaddress.IPv4Address)
    except ValueError:
        return False


def convert_to_ip4(value) -> str | None:
    """value строка ip4 адресом или доменным именем"""
    if is_ip4(value):
        return value
    if ip4 := get_ip4s(value):
        return ip4
    raise Exception(f"Incorrect value {value} for host")
