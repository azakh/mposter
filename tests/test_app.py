import pytest
from fastapi.testclient import TestClient

from app.main import app

client = TestClient(app)


@pytest.mark.parametrize(
    "src,dst",
    [
        pytest.param(
            dict(host="imap.mail.ru", user="andrew.zakharov@internet.ru", password="npTwvAmtAWqxMVtmeZYK"),
            dict(host="imap.yandex.ru", user="myaccountfortest@yandex.ru", password="myaccountfortest1"),
            id="localhost",
        ),
    ],
)
@pytest.mark.asyncio
async def test_read_main(
    src,
    dst,
):
    response = client.get("/", json={"src": src, "dst": dst})
    assert response.status_code == 200
