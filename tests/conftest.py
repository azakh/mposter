import argparse
import pathlib
from collections import namedtuple

import pytest

EmailRow = namedtuple("EmailRow", "email, separator, password, error_text")


@pytest.fixture
def domain():
    return "example.com"


@pytest.fixture(
    scope="function",
    params=[
        pytest.param(
            [
                "_",
                "-s",
                "127.0.0.1",
                "-d",
                "127.0.0.1",
                "-f",
            ],
            id="with_ips",
        ),
        pytest.param(
            [
                "_",
                "-s",
                "localhost",
                "-d",
                "localhost",
                "-f",
            ],
            id="with_domains",
        ),
    ],
)
def arguments(request, file_name):
    request.param.append(file_name)
    return request.param


@pytest.fixture(
    scope="function",
    params=[
        pytest.param(["_", "-s", "127.0.0.1", "-d", "127.0.0.1", "-f", "/home/andrew/mposter/file.txt"], id="with_ips"),
        pytest.param(
            ["_", "-s", "localhost", "-d", "localhost", "-f", "/home/andrew/mposter/file2.txt"], id="with_domains"
        ),
    ],
)
def bad_arguments(request):
    return request.param


@pytest.fixture(scope="function")
def file_name():
    return "file_name.txt"


@pytest.fixture(
    scope="function",
    params=[
        pytest.param(
            EmailRow("ema il@ru.ru", ":", "fheehffej", "Incorrect email format in file_name.txt:1"),
            id="Incorrect_email",
        ),
        pytest.param(
            EmailRow("email@ru.ru", ")", "fheehffej", "Incorrect separator"),
            id="Incorrect_separator",
        ),
    ],
)
def bad_data_in_file(file_name, request):
    with open(file_name, "w", 1, "utf8") as f:
        request = request.param
        f.write(request.email + request.separator + request.password + "\n")
        yield request
        pathlib.Path(file_name).unlink()
