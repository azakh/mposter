from datetime import datetime, timezone, timedelta
from socket import gaierror  # pylint: disable=no-name-in-module
from contextlib import nullcontext as does_not_raise
import pytest
import pytest_mock  # pylint: disable=unused-import
from pytest_mock import MockerFixture  # pylint: disable=unused-import
from network import get_ip4s  # pylint: disable=import-error
from imap_actors.mail_providers import MailRu  # pylint: disable=import-error


@pytest.mark.parametrize(
    "address,expected",
    [
        pytest.param("localhost", "127.0.0.1", marks=pytest.mark.basic),
        pytest.param("127.0.0.1", "127.0.0.1", marks=pytest.mark.basic),
    ],
)
def test_get_ip4s_with_domain(address, expected):
    assert get_ip4s(address) == expected


@pytest.mark.parametrize(
    "inp,expectation",
    [
        pytest.param("localhost", does_not_raise(), id="localhost"),
        pytest.param("127.0.0.1", does_not_raise(), id="127.0.0.1"),
        pytest.param(0.0, pytest.raises(TypeError), id="wrong type params"),
        pytest.param("0.0.0.00.", pytest.raises(gaierror), id="wrong data format"),
    ],
)
def test_get_ip4s_on_exception(inp, expectation):
    with expectation:
        get_ip4s(inp)


def test_get_ip4s_with_ip(domain):
    assert "0.0.0.0" == get_ip4s(domain)


def test_get_ip4s_with_incorrect_data():
    with pytest.raises(gaierror):
        get_ip4s("0.0.0.0.0")


def test_parse_date_time():
    ti = "Tue, 16 Aug 2022 15:35:23 +0300"
    res = datetime(2022, 8, 16, 15, 35, 23, tzinfo=timezone(offset=timedelta(minutes=+300)))
    test = MailRu.parse_date_time(ti)
    print(test == res)
    assert MailRu.parse_date_time(ti) == res
