from pydantic import BaseModel, Field


class Creeds(BaseModel):
    host: str = Field(description="ipv4 or DNS domain")
    user: str
    password: str
