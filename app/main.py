from fastapi import FastAPI, BackgroundTasks
from imap_actors.mail_providers import Creeds, do_transmission
from .models.creeds import Creeds as CreedsValidator

app = FastAPI()


@app.get("/")
async def main(src: CreedsValidator, dst: CreedsValidator, background_tasks: BackgroundTasks):
    src = Creeds(host=src.host, user=src.user, password=src.password)
    dst = Creeds(host=dst.host, user=dst.user, password=dst.password)
    background_tasks.add_task(do_transmission, src, dst)
    return {"message": "Notification sent in the background"}
