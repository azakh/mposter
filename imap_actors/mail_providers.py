import abc
import collections
import hashlib
import re
from abc import ABC
import email
import functools
import ssl
import logging
import sys
from collections import namedtuple
from datetime import datetime, timezone, timedelta
import aioimaplib
from network import convert_to_ip4
from imap_actors import utf7_tool


register: dict[str:"ImapClient"] = {}

logger = logging.getLogger(__file__)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
logger.addHandler(handler)
logger.setLevel(logging.INFO)
Email = namedtuple("Email", ("data", "date", "flags"))
Creeds = namedtuple("Creeds", ("host", "user", "password"))


def create_ssl_context(
    proto=ssl.PROTOCOL_SSLv23, verify_mode=ssl.CERT_NONE, protocols=None, options=None, ciphers="ALL"
):
    protocols = protocols or ("PROTOCOL_SSLv3", "PROTOCOL_TLSv1", "PROTOCOL_TLSv1_1", "PROTOCOL_TLSv1_2")
    options = options or (
        "OP_CIPHER_SERVER_PREFERENCE",
        "OP_SINGLE_DH_USE",
        "OP_SINGLE_ECDH_USE",
        "OP_NO_COMPRESSION",
    )
    context = ssl.SSLContext(proto)
    context.verify_mode = verify_mode
    # reset protocol, options
    for o in options:
        context.options |= getattr(ssl, o, 0)
    context.set_ciphers(ciphers)
    return context


async def do_transmission(src_con: Creeds, dst_con: Creeds):
    src = get_imap_client(imap_creeds=src_con)
    await src.login(src_con.user, src_con.password)
    dst = get_imap_client(imap_creeds=dst_con)
    await dst.login(dst_con.user, dst_con.password)

    src_separator, src_mailboxes = await src.get_separator_and_mailboxes()
    dst_separator, dst_mailboxes = await dst.get_separator_and_mailboxes()
    base_copy_folder = utf7_tool.imap_utf7encode(f"Перенос почты для {src_con.user}")

    await dst.copy_mailboxes(base_copy_folder, src_separator, dst_separator, src_mailboxes)
    for mbx in src_mailboxes:
        seen = await src.seen_uids_set(mailbox=mbx)
        dst_mbx = dst.convert_mbx_format(base_copy_folder, dst_separator, mbx, src_separator)
        for uid in await src.get_letter_uids_from_mailbox(mbx):
            if uid not in dst.checkpoint[mbx]:
                eml = await src.get_email(uid, mbx, r"\SEEN" if uid in seen else r"\UNSEEN")
                await dst.append_email(eml, dst_mbx=dst_mbx)
                dst.checkpoint_add(mbx=mbx, uid=uid)

    await src.logout()
    await dst.logout()


def hash_email(mbx: str, uid: str) -> str:
    return mbx + uid


class ImapClient(ABC):

    imap_domain = "imap.yandex.ru"
    imap_client: aioimaplib.IMAP4_SSL
    hashes = set()
    current_mbx: str
    checkpoint: dict[str : set[int]]

    def checkpoint_add(self, mbx, uid):
        self.checkpoint[mbx].add(uid)

    def convert_mbx_format(self, base_copy_folder: str, dst_separator: str, mbx: str, src_separator: str):
        return '"' + base_copy_folder + dst_separator + mbx.replace(src_separator, dst_separator).strip('" ') + '"'

    async def copy_mailboxes(
        self, base_copy_folder: str, src_separator: str, dst_separator: str, src_mailboxes: list[str]
    ):
        """'
        () "/" "INBOX/Social"' -> () "." "base_copy_folder.INBOX.Social"'
        """
        await self.imap_client.select("INBOX")
        res = await self.imap_client.create('"' + base_copy_folder + '"')
        for mbx in src_mailboxes:
            dst_mbx = self.convert_mbx_format(base_copy_folder, dst_separator, mbx, src_separator)
            res = await self.imap_client.create(dst_mbx)

    def __init_subclass__(cls, **kwargs):
        register[convert_to_ip4(cls.imap_domain)] = cls

    def __init__(self, host: str, port: int = aioimaplib.IMAP4_SSL_PORT):
        self.checkpoint = collections.defaultdict(set)

    @abc.abstractmethod
    async def logout(self):
        ...

    @abc.abstractmethod
    async def login(self, user: str, password: str):
        ...

    @abc.abstractmethod
    async def get_separator_and_mailboxes(self) -> list[str]:
        ...

    @abc.abstractmethod
    async def get_letter_uids_from_mailbox(self, maolbox: str) -> list[str]:
        ...

    @staticmethod
    @abc.abstractmethod
    def parse_date_time(date_time: str) -> datetime:
        ...

    @abc.abstractmethod
    async def get_email(self, uid, mbx, flag):
        ...

    @abc.abstractmethod
    async def all_uids(self):
        ...

    @abc.abstractmethod
    @functools.lru_cache(maxsize=1)
    async def seen_uids_set(self, mailbox: str):
        ...

    @abc.abstractmethod
    async def create_folder(self, mailbox_name: str):
        ...

    async def append_email(self, email: Email, dst_mbx):

        logger.info(hashlib.md5(email.data).hexdigest())
        res = await self.imap_client.append(
            message_bytes=email.data,
            mailbox=dst_mbx,
            flags=email.flags,
            date=email.date,
        )
        logger.info(res)

    @abc.abstractmethod
    async def get_emails(self) -> [Email, ...]:
        ...


class DefaultImapClient(ImapClient):
    def __init__(self, host: str, port: int = aioimaplib.IMAP4_SSL_PORT):
        super().__init__(host, port)
        self.imap_client = aioimaplib.IMAP4_SSL(host=host, port=port, timeout=30, ssl_context=create_ssl_context())
        self.transfer_done = False

    async def logout(self):
        ...

    async def login(self, user: str, password: str):
        ...

    async def get_separator_and_mailboxes(self) -> list[str]:
        ...

    async def get_letter_uids_from_mailbox(self, maolbox: str) -> list[str]:
        ...

    @staticmethod
    def parse_date_time(date_time: str) -> datetime:
        ...

    async def get_email(self, uid, mbx, flag) -> Email:
        ...

    async def all_uids(self):
        ...

    async def seen_uids_set(self, mailbox: str):
        ...

    async def create_folder(self, mailbox_name: str):
        ...

    async def append_email(self, email: Email, dst_mbx=None):
        ...

    async def get_emails(self) -> [Email, ...]:
        ...


class MailRu(ImapClient):
    imap_domain = "imap.mail.ru"
    imap_client: aioimaplib.IMAP4_SSL
    hashes = set()
    current_mbx: str
    separator: str
    checkpoint: dict[str : set[int]]

    def __init__(self, host: str, port: int = aioimaplib.IMAP4_SSL_PORT):
        super().__init__(host, port)
        self._host = host
        self.imap_client = aioimaplib.IMAP4_SSL(host=host, port=port, timeout=30, ssl_context=create_ssl_context())

    async def logout(self):
        await self.imap_client.logout()

    async def login(self, user: str, password: str):
        await self.imap_client.wait_hello_from_server()
        r = await self.imap_client.login(user, password)
        if r.result == "NO":
            raise Exception(r.lines[0])
        await self.imap_client.select("INBOX")

    async def get_separator_and_mailboxes(self) -> tuple[str, list[str]]:
        resp = await self.imap_client.list('""', "*")
        mailboxes = []

        if separator_with_quotes := re.template(r'[\'"].[\'"]').findall(resp.lines[0].decode()):
            separator_with_quotes = separator_with_quotes[0]
            separator = separator_with_quotes.strip("'\"")
        else:
            raise Exception(f"Not found separator for IMAP server {self._host}")

        for mbx in resp.lines[:-1]:
            if mailbox := mbx.decode().split(separator_with_quotes):
                mailboxes.append(mailbox[-1])
        assert "separator" in locals()
        return separator, mailboxes

    async def get_letter_uids_from_mailbox(self, maolbox: str) -> list[str]:
        await self.imap_client.select(maolbox)
        res, data = await self.imap_client.search("ALL")
        uids = data[0].split()
        return [i.decode() for i in uids]

    @staticmethod
    def parse_date_time(date_time: str) -> datetime:

        m = {
            "jan": 1,
            "feb": 2,
            "mar": 3,
            "apr": 4,
            "may": 5,
            "jun": 6,
            "jul": 7,
            "aug": 8,
            "sep": 9,
            "oct": 10,
            "nov": 11,
            "dec": 12,
        }

        day_of_month = slice(4, 7)
        moth = slice(8, 11)
        year = slice(12, 16)
        hour = slice(17, 19)
        minute = slice(20, 22)
        second = slice(23, 25)
        tz = slice(26, 32)
        return datetime(
            year=int(date_time[year]),
            month=m[date_time[moth].lower()],
            day=int(date_time[day_of_month]),
            hour=int(date_time[hour]),
            minute=int(date_time[minute]),
            second=int(date_time[second]),
            tzinfo=timezone(timedelta(minutes=int(date_time[tz]))),
        )

    async def get_email(self, uid, mbx, flag):
        if answer := await self.imap_client.fetch(uid, "(RFC822)"):
            if len(answer.lines) == 4:
                res, (_, data, _, _) = answer
                if res == "OK":
                    if hash_email(mbx, uid) not in self.hashes:
                        date = self.parse_date_time(email.message_from_bytes(data).get("Date"))
                        self.hashes.add(hash_email(mbx, uid))
                        return Email(
                            data=data,
                            date=date,
                            flags=flag,
                        )

    async def all_uids(self):
        res, data = await self.imap_client.search("ALL")
        uids = [i.decode() for i in data[0].split()]
        return uids

    @functools.lru_cache(maxsize=1)
    async def seen_uids_set(self, mailbox: str):
        res, data = await self.imap_client.uid_search(r"SEEN")
        return {i.decode() for i in data[0].split()}

    async def create_folder(self, mailbox_name: str):
        await self.imap_client.create(mailbox_name)

    async def get_emails(self) -> [Email, ...]:
        cur_mbx = None
        for mbx in await self.get_separator_and_mailboxes():
            await self.imap_client.select(mbx)
            if cur_mbx != mbx:
                cur_mbx = mbx
            seen = await self.seen_uids_set(cur_mbx)
            for uid in await self.all_uids():
                yield await self.get_email(uid, mbx, r"\SEEN" if uid in seen else r"\UNSEEN")

            print("Fin")


class YaRu(ImapClient):
    imap_domain = "imap.yandex.ru"
    imap_client: aioimaplib.IMAP4_SSL
    hashes = set()
    transfer_done: bool
    current_mbx: str
    checkpoint: dict[str : set[int]]

    def __init__(self, host: str, port: int = aioimaplib.IMAP4_SSL_PORT):
        super().__init__(host, port)
        self.imap_client = aioimaplib.IMAP4_SSL(host=host, port=port, timeout=30, ssl_context=create_ssl_context())
        self.transfer_done = False

    async def logout(self):
        await self.imap_client.logout()

    async def login(self, user: str, password: str):
        await self.imap_client.wait_hello_from_server()
        r = await self.imap_client.login(user, password)
        if r.result == "NO":
            raise Exception(r.lines[0])
        await self.imap_client.select("INBOX")

    async def get_separator_and_mailboxes(self) -> tuple[str, list[str]]:
        resp = await self.imap_client.list('""', "*")
        mailboxes = []

        if separator_with_quotes := re.template(r'[\'"].[\'"]').findall(resp.lines[0].decode()):
            separator_with_quotes = separator_with_quotes[0]
            separator = separator_with_quotes.strip("'\"")
        else:
            raise Exception(f"Not found separator for IMAP server {self._host}")

        for mbx in resp.lines[:-1]:
            if mailbox := mbx.decode().split(separator_with_quotes):
                mailboxes.append(mailbox[-1])
        assert "separator" in locals()
        return separator, mailboxes

    async def get_letter_uids_from_mailbox(self, maolbox: str) -> list[str]:

        await self.imap_client.select(maolbox)
        res, data = await self.imap_client.search("ALL")
        uids = [i.decode() for i in data[0].split()]
        return uids

    @staticmethod
    def parse_date_time(date_time: str) -> datetime:

        m = {
            "jan": 1,
            "feb": 2,
            "mar": 3,
            "apr": 4,
            "may": 5,
            "jun": 6,
            "jul": 7,
            "aug": 8,
            "sep": 9,
            "oct": 10,
            "nov": 11,
            "dec": 12,
        }

        day_of_month = slice(4, 7)
        moth = slice(8, 11)
        year = slice(12, 16)
        hour = slice(17, 19)
        minute = slice(20, 22)
        second = slice(23, 25)
        tz = slice(26, 32)
        return datetime(
            year=int(date_time[year]),
            month=m[date_time[moth].lower()],
            day=int(date_time[day_of_month]),
            hour=int(date_time[hour]),
            minute=int(date_time[minute]),
            second=int(date_time[second]),
            tzinfo=timezone(timedelta(minutes=int(date_time[tz]))),
        )

    async def get_email(self, uid, mbx, flag):
        if answer := await self.imap_client.fetch(uid, "(RFC822)"):
            if len(answer.lines) == 4:
                res, (_, data, _, _) = answer
                if res == "OK":
                    if hash_email(mbx, uid) not in self.hashes:
                        date = self.parse_date_time(email.message_from_bytes(data).get("Date"))
                        self.hashes.add(hash_email(mbx, uid))
                        return Email(
                            data=data,
                            date=date,
                            flags=flag,
                        )

    async def all_uids(self):
        res, data = await self.imap_client.search("ALL")
        uids = [i.decode() for i in data[0].split()]
        return uids

    @functools.lru_cache(maxsize=1)
    async def seen_uids_set(self, mailbox: str):
        res, data = await self.imap_client.uid_search(r"SEEN")
        return {i.decode() for i in data[0].split()}

    async def create_folder(self, mailbox_name: str):
        await self.imap_client.create(mailbox_name)

    async def get_emails(self) -> [Email, ...]:
        cur_mbx = None
        for mbx in await self.get_separator_and_mailboxes():
            await self.imap_client.select(mbx)
            if cur_mbx != mbx:
                cur_mbx = mbx
            seen = await self.seen_uids_set(cur_mbx)
            for uid in await self.all_uids():
                yield await self.get_email(uid, mbx, r"\SEEN" if uid in seen else r"\UNSEEN")

            print("Fin")


def get_imap_client(imap_creeds: Creeds) -> ImapClient | DefaultImapClient:

    if correct_ip_address := convert_to_ip4(imap_creeds.host):
        if imap_client := register.get(correct_ip_address):
            return imap_client(host=imap_creeds.host, port=993)
        return DefaultImapClient(host=imap_creeds.host, port=993)

    raise Exception(f"Incorrect resolving {imap_creeds.host} in DNS")
